## Genetic Framework

A simple and easily expandable C++ genetic algorithms framework

Project structure:
 * bin: output executables go here
 * build: object file
 * include: all the header files
 * src: this framework src files
 * input: the input files with instances
 * tools: aux tools to understand the problem

# Two-dimensional free cut oriented packing
The two-dimensional bin packing is an NP-hard problem that aims to minimize the number of bins needed to pack a given set of rectangular shapes of a given size.

# Genetic ALgorithms (GA)
A meta-heuristic GA is an algorithm that uses concepts from genetics to search for a solution for an optimization problem. Each generation passage implies sexual reproduction (cross-over), which is implemented as a cut from a parent genome, filled up by the other parent genes, avoid repetition and omission since the genome here represents the permutation in which the items should be packed by the bottom-left-first heuristic.

## Bottom-left-fist heuristic
The 2D bin packing can be expressed in a two-phase algorithm: first, we determine a permutation of all items available, then we try to put each one in a bin, bottom first, until the next item can't fit in the remaining width. When this happens, the next level, above the highest item put so far, is tested. When it is impossible to pack the next item in the current bin, i.e. remaining hight is too low, ask for another bin.

# Darwinian Islands
The Darwinian Island variation that must be implemented here is as follow: the population will be divided into N regions (islands), each process of crossing-over should happen only between individuals of the same region. Assuming that the program will run until G generations passed, then X times, X lesser than G, at generations g1, g2 ... gx the fittest individual of each island i should migrate to the next island.
More detail in: https://conf.ztu.edu.ua/wp-content/uploads/2017/05/122.pdf

# visuBin
It is possible the visualize the output using the visuBin tool, available in the tools folder. The needed instructions lie in ```tools/visuBin/README.md```, but all you have to do is run getVisuals.sh. The output will be in ```tools/visuBin/output```.

# Code Usage
To compile the code use ```make ```. The executables will be in the ```bin/``` folder. Just use ```bin/genetic_framework``` to run the code. Some information, such as the running time, will be displayed on the standar output. The result will be stored on ```output/packing_info/``` folder.

The main function chooses witch instance must be solved: 

```c
int main()
{
    /* seeds the pseudo-random number generator */
    set_seed(time(NULL));

    /* Queue everything we wanna run */
    GAQueue instQueue;

    /*
     * Params:
     * Problem class
     * Problem instance
     * Population size
     * Number of generations
     * Mutation Rate
     * Time limit in seconds
     */
    instQueue.push(1, 49, 1500,5000,5,180);

    /* run everything */
    while(!instQueue.isEmpty()){
        instQueue.runNext();
    }

    return 0;
}
```

The line :
```c
instQueue.push(1, 49, 1500,5000,5,180);
```
queues the instance 49, present in the class 1 files. Note that you don't have to change the instance to measure speed-up, but you can, if you wish so.

```c
instQueue.push(x, y, 1500,5000,5,180);
```
Any combination of class X and instance Y is acceped, if avaliable on the folder ```input/```.


# Todo
* Implement Darwinian Islands parallelization at method DefaultGA::islandSolve(). 
  * Hint: each generation depends on the previous and on the one migration that must be done after half of the number of generations have passed.
  * Hint : you only have to modify the method ```DeafultGA::islandSolve()```.

Running the code will display the running time of both methods: the serial and the parallel, as in:
```
Starting SERIAL solver for C01_i49_1500popsize_5000gen_5mr_180s
Number of bins used: 39
Elasped time is 41.0 seconds.
Number of generations: 4999
Buffer used: [5671]/[8000] (70.89%)
=================
=====================
Starting PARALLEL solver for C01_i49_1500popsize_5000gen_5mr_180s
Number of bins used: 40
Elasped time is 40.0 seconds.
Number of generations: 4999
Buffer used: [5715]/[8000] (71.44%)
```

