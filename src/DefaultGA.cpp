#include "DefaultGA.h"
#include <omp.h>

DefaultGA::DefaultGA(int popSize, int generations, int mutationRate, int chromosomeSize, BotLeftHeuristic instance) : PlaceHeuristic(instance)
{
    DefaultGA::generations = generations;
    DefaultGA::popSize = popSize;
    DefaultGA::mutationRate = mutationRate;
    DefaultGA::chromosomeSize = chromosomeSize;
    bestChromossome =(int*)malloc(sizeof(int) * chromosomeSize);
}

DefaultGA::~DefaultGA(void){
    free(bestChromossome);
}

/* Ordered crossover
 * this method produces a valid permutation
 * by selecting a random subset from the first
 * parent and then filling the missing parts
 * with the values from the other parent,
 * in the order that they appear
 * */
void DefaultGA::crossoverOrdered(int *parentA, int *parentB,
        int *childA, int *childB){

    int size = DefaultGA::chromosomeSize;

    /*
     * A: x1 x2 x3 x4 x5
     *            ^
     * B: x3 x5 x2 x4 x1
     *
     * c: x1 x2 x3 | x5 x4
     *============================
     * x1 x2 x3 where copied from A and
     * x4 and x5 where the remaning allels
     * so they where added in the order that
     * they appear in B
     */
    int slice_pos = get_random(1, size-2);
    int missing_elementA[size];
    int missing_elementB[size];

    //printf("position of cross: %d\n", slice_pos);
    /* inicializes vetor of missing elements */
    for(int i=0; i < size; i++) {
        missing_elementA[i] = 1;
        missing_elementB[i] = 1;
    }

    for(int i = 0; i < slice_pos; i++){
        /* indicates this element is already in
         * the child */
        missing_elementA[parentA[i]] = 0;
        childA[i] = parentA[i];

        missing_elementB[parentB[i]] = 0;
        childB[i] = parentB[i];
    }

    /* completes the CHILDREN */
    int childAWritePos = slice_pos;
    int childBWritePos = slice_pos;
    for(int i = 0; i < size; i++){
        if (missing_elementA[parentB[i]] == 1)
            childA[childAWritePos++] = parentB[i];

        if (missing_elementB[parentA[i]] == 1)
            childB[childBWritePos++] = parentA[i];
    }

}

// mutate a sequence of locus
// 2 4 1 3 5 0
//x   x         mutation points
// result = 1 3 5 0 2 4
void DefaultGA::sequenceMutate(int *chromosome, int rate) {
    int size = DefaultGA::chromosomeSize;

    int dice = get_random(1,100);
    if (dice > rate) {
        int slice_pos1 = get_random(0, size-1);
        int slice_pos2 = get_random(slice_pos1, size-1);
        //printf("pos1 == %d \tpos2 ==%d\n", slice_pos1, slice_pos2);
        int mutate_chromosome[size];
        int j = 0;
        for (int i = 0; i < slice_pos1; i++) {
            mutate_chromosome[j] = chromosome[i];
            j++;
        }
        for (int i = slice_pos2; i < size; i++) {

            mutate_chromosome[j] = chromosome[i];
            j++;
        }
        for (int i = slice_pos1; i < slice_pos2; i++) {
            mutate_chromosome[j] = chromosome[i];
            j++;
        }

        for (int i = 0; i < size; i++) {
            chromosome[i] = mutate_chromosome[i];
        }
    }
}

void DefaultGA::generateInitialPopulation(int **population, int popSize) {
    for(int i = 0; i < popSize; i++)
        DefaultGA::generatePermutation(population[i]);
}

void DefaultGA::elitistGeneration (int **population, int **offspring, int popSize) {
    int bestFit = 0;
    int fit;
    int fittest[DefaultGA::chromosomeSize];
    // find the fittest chromosome from the previous population
    for (int i = 0; i < popSize; i++) {
        fit = fitness(population[i]);
        if (fit > bestFit) {
            bestFit = fit;
            memcpy(fittest, population[i], DefaultGA::chromosomeSize*sizeof(int));

        }
    }

    // add the fittest to the new population
    memcpy(population[0], fittest, DefaultGA::chromosomeSize*sizeof(int));

    // add the other popSize - 1 chromosomes to the new population
    for (int i = 1; i < popSize; i++) {
        // memcpy(population[i], offspring[i], DefaultGA::chromosomeSize*sizeof(char));
        for (int j = 0; j < DefaultGA::chromosomeSize; j++) {
            population[i][j] = offspring[i][j];
        }
    }
}

/* generate a random chromosome */
void DefaultGA::generatePermutation(int *chromosome){
    int size = DefaultGA::chromosomeSize;

    /*
     * generates a chromosome
     * with the permutation:
     * 0,1,2,...,size
     */
    for(int i=0;i<size;i++)
        chromosome[i]=i;

    /*
     * adds some entropy to the
     * generate chromosome,
     * by mutating it with a high
     * mutation rate four times
     */

    DefaultGA::sequenceMutate(chromosome, 100);
    DefaultGA::sequenceMutate(chromosome, 100);
    DefaultGA::sequenceMutate(chromosome, 100);
    DefaultGA::sequenceMutate(chromosome, 100);
}


/*
 * Choses K random individuals from the
 * current population and cretes two groups.
 * The fittest wins and is selected to be a
 * parent. This process runs twice, thus
 * selecting two parents.
 */
void DefaultGA::tournamentSelection(int **population, int *parentA,
        int *parentB, int tournament_dimension, int size)  {
    int participants[tournament_dimension][DefaultGA::chromosomeSize];

    /* the first tournament */
    for (int i = 0; i < tournament_dimension; i++) {
        int rng = get_random(0, size - 1);
        memcpy(participants[i], population[rng], DefaultGA::chromosomeSize*sizeof(int));
    }
    int j = 0;
    int bestFit = 0;
    for (int i = 0; i < tournament_dimension; i++) {
        int fit = fitness(participants[i]);
        if (fit > bestFit) {
            j = i;
            bestFit = fit;
        }
    }

    memcpy(parentA, participants[j], DefaultGA::chromosomeSize*sizeof(int));

    for (int i = 0; i < tournament_dimension; i++) {
        int rng = get_random(0, size - 1);
        memcpy(participants[i], population[rng], DefaultGA::chromosomeSize*sizeof(int));
    }

    j = 0;
    bestFit = 0;
    for (int i = 0; i < tournament_dimension; i++) {
        int fit = fitness(participants[i]);    // confio em voce compilador *shoulder tap*
        if (fit > bestFit) {
            j = i;
            bestFit = fit;
        }
    }

    memcpy(parentB, participants[j], DefaultGA::chromosomeSize*sizeof(int));
}

/*
 * This function prints all the individuals of the
 * current pupulation in the standard output.
 */
void DefaultGA::showPopulation(int **population, int popSize){
    for(int i = 0; i < popSize; i++){
        printf("Individual %d = [ ", i);
        for(int temp = 0; temp < chromosomeSize; temp++)
            printf("%d ",population[i][temp]);
        printf("]\n");
    }
}

/*
 * This function prints the given chromosome
 * in the standard output.
 */
void DefaultGA::showChromosome(int *chromosome) {
    printf("Individual = [ ");
    for(int temp = 0; temp < chromosomeSize; temp++)
        printf("%d ",chromosome[temp]);
    printf("]\n");
}


/*
 * Calculates the fitness of a given
 * chromosome by calculating the number of
 * bins needed to pack the items using
 * this permutation.
 * Lesser bins used means higher fitness
 */
int DefaultGA::fitness (int *chromosome) {
    int numOfBins = DefaultGA::PlaceHeuristic.numOfBins(chromosome);
    int numOfItens = DefaultGA::PlaceHeuristic.getNumOfItens();
    return numOfItens-numOfBins;
}


int* DefaultGA::getBestChromossome(int **population, int popSize){
    int i =  DefaultGA::getBestChromossomePos(population, popSize);

    return population[i];

}

int DefaultGA::getBestChromossomePos(int **population, int popSize){
    int bestFitnessChromosomePos = 0;
    int currentFitnessIsland1 = DefaultGA::fitness(population[0]);

    for(int i = 1; i < popSize; i++){
        if(DefaultGA::fitness(population[i]) > currentFitnessIsland1){
            bestFitnessChromosomePos = i;
        }
    }

    return bestFitnessChromosomePos;
}

int *DefaultGA::getBestChromossomeOf4islands(int **island1, int **island2, int **island3, int **island4, int popSize){
    //int bestOfisland1 = DefaultGA::getBestChromossomePos(island1);

    int fit1, fit2, fit3, fit4;
    int bestOf1, bestOf2, bestOf3, bestOf4;

    bestOf1 = DefaultGA::getBestChromossomePos(island1, popSize);
    bestOf2 = DefaultGA::getBestChromossomePos(island2, popSize);
    bestOf3 = DefaultGA::getBestChromossomePos(island3, popSize);
    bestOf4 = DefaultGA::getBestChromossomePos(island4, popSize);

    fit1 = DefaultGA::fitness(island1[bestOf1]);
    fit2 = DefaultGA::fitness(island2[bestOf2]);
    fit3 = DefaultGA::fitness(island3[bestOf3]);
    fit4 = DefaultGA::fitness(island4[bestOf4]);

    if(fit1 >= fit2 && fit1 >= fit3 && fit1 >= fit4)
        return island1[bestOf1];

    if(fit2 >= fit1 && fit2 >= fit3 && fit2 >= fit4)
        return island2[bestOf2];

    if(fit3 >= fit1 && fit3 >= fit2 && fit3 >= fit4)
        return island3[bestOf3];

    return island4[bestOf4];
}

int *DefaultGA::getFinalBestChromossome(){
    return DefaultGA::bestChromossome;
}

int DefaultGA::getNumOfPassedGens(){
    return DefaultGA::passedGens;
}

/*
 * Four island model migration:
 * when this function is called, it takes
 * the best individual from island i and
 * moves him to insland i + 1 mod 4
 *
 * i.e.:
 *   best of island 1 ->> island 2
 *   best of island 2 ->> island 3
 *   best of island 3 ->> island 4
 *   best of island 4 ->> island 1
 * */
void DefaultGA::fourIslandsMigration(int **island1, int **island2, int **island3, int **island4, int islandSize){
    int *bestOfIsl1 = getBestChromossome(island1, islandSize);
    int *bestOfIsl2 = getBestChromossome(island2, islandSize);
    int *bestOfIsl3 = getBestChromossome(island3, islandSize);
    int *bestOfIsl4 = getBestChromossome(island4, islandSize);

    int *temp = (int*)malloc(sizeof(int) * chromosomeSize);

    /*
     * temp < bestOf4
     * i.e.: we are saving the best chromossome from
     * islad 4
     * */
    memcpy(temp, bestOfIsl4, sizeof(int) * chromosomeSize);

    /* island 4 recieves from island 3*/
    memcpy(bestOfIsl4, bestOfIsl3, sizeof(int) * chromosomeSize);

    /* island 3 recieves from island 2*/
    memcpy(bestOfIsl3, bestOfIsl2, sizeof(int) * chromosomeSize);

    /* island 3 recieves from island 1*/
    memcpy(bestOfIsl3, bestOfIsl1, sizeof(int) * chromosomeSize);

    /*
     * island 1 recieves from island 4
     * but since we have already overwritten
     * island 4, we take its previous value
     * from temp
     * */
    memcpy(bestOfIsl1, temp, sizeof(int) * chromosomeSize);

    free(temp);
}

/*
 * Runs an generation in an
 * given island
 * 
 * */
void DefaultGA::runIslandGeneration(int **islandPop, int **islandOffspring, int popSize, int *bestIndividual) {
/* generating offspring */
        for(int i = 0; i < popSize; i = i + 2){
            /* allocates parents to-be chromossomes */
            int parentA[DefaultGA::chromosomeSize];
            int parentB[DefaultGA::chromosomeSize];

            /* selects the parents */
            DefaultGA::tournamentSelection(islandPop, parentA, parentB, 5, popSize);

            /* does sexual reprouction for
             * selected parents */
            DefaultGA::crossoverOrdered(parentA, parentB, islandOffspring[i], islandOffspring[i+ 1]);

        }

        /* apply mutation to the offspring*/
        for(int i = 0; i < popSize; i++)
            DefaultGA::sequenceMutate((int*)islandOffspring[i], DefaultGA::mutationRate);

        /*
         * Creates the offspring from current population
         */
        DefaultGA::elitistGeneration(islandPop,islandOffspring, popSize);

}

/*
 * Solves an instance of GA,
 * returning the best individual
 */
void DefaultGA::solve(int max_time_seconds){
    /* splits the population in  groups */
    int popSize = (int)DefaultGA::popSize/4;

    /* limitation: popSize must be pair */
    if(popSize % 2)
        popSize += 1;

    int chromosomeSize = DefaultGA::chromosomeSize;
    int generations = DefaultGA::generations;

    int **island1;
    int **island2;
    int **island3;
    int **island4;

    island1 = (int**)malloc(sizeof(int*) * popSize);
    island2 = (int**)malloc(sizeof(int*) * popSize);
    island3 = (int**)malloc(sizeof(int*) * popSize);
    island4 = (int**)malloc(sizeof(int*) * popSize);

    for(int i = 0; i < popSize; i++){
        island1[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        island2[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        island3[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        island4[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }

    DefaultGA::generateInitialPopulation(island1, popSize);
    DefaultGA::generateInitialPopulation(island2, popSize);
    DefaultGA::generateInitialPopulation(island3, popSize);
    DefaultGA::generateInitialPopulation(island4, popSize);
    int *bestChromossomeInIsland1 = island1[0];
    int *bestChromossomeInIsland2 = island2[0];
    int *bestChromossomeInIsland3 = island3[0];
    int *bestChromossomeInIsland4 = island4[0];

    /* generates the inicial population */
    /* and allocates memory for offspring calculations */
    int **offspring1 = (int**)malloc(popSize * sizeof(int*));
    int **offspring2 = (int**)malloc(popSize * sizeof(int*));
    int **offspring3 = (int**)malloc(popSize * sizeof(int*));
    int **offspring4 = (int**)malloc(popSize * sizeof(int*));

    for(int i = 0; i < popSize; i++){
        offspring1[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        offspring2[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        offspring3[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        offspring4[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }

    /*
     * Variables to control elapsed time
     * and elapsed number of generations
     * */
    time_t start_time,current_time;
    time (&start_time);
    int elapsed = 0;
    int elapsed_gens = 0;

    for(int i = 0; i < generations/2 && elapsed <= max_time_seconds; i++){
        /* run the generation method */
        runIslandGeneration(island1, offspring1, popSize, bestChromossomeInIsland1);
        runIslandGeneration(island2, offspring2, popSize, bestChromossomeInIsland2);
        runIslandGeneration(island3, offspring3, popSize, bestChromossomeInIsland3);
        runIslandGeneration(island4, offspring4, popSize, bestChromossomeInIsland4);

        /*  update elapsed time and number of gens */
        time (&current_time);
        elapsed = (int) difftime (current_time,start_time);
        elapsed_gens = i;
    }

    /* does a migration */
    fourIslandsMigration(island1, island2, island3, island4, popSize);

    for(int i = generations/2; i < generations && elapsed <= max_time_seconds; i++){
        /* run the generation method */
        runIslandGeneration(island1, offspring1, popSize, bestChromossomeInIsland1);
        runIslandGeneration(island2, offspring2, popSize, bestChromossomeInIsland2);
        runIslandGeneration(island3, offspring3, popSize, bestChromossomeInIsland3);
        runIslandGeneration(island4, offspring4, popSize, bestChromossomeInIsland4);

        /*  update elapsed time and number of gens */
        time (&current_time);
        elapsed = (int) difftime (current_time,start_time);
        elapsed_gens = i;
    }

    DefaultGA::passedGens = elapsed_gens;

    memcpy(DefaultGA::bestChromossome, DefaultGA::getBestChromossomeOf4islands(island1, island2, island3, island4, popSize),
           DefaultGA::chromosomeSize*sizeof(int));

    /* free offspring allocated memory */
    for(int i = 0; i < popSize; i++){
        free(offspring1[i]);
        free(offspring2[i]);
        free(offspring3[i]);
        free(offspring4[i]);
    }
    free(offspring1);
    free(offspring2);
    free(offspring3);
    free(offspring4);

    /* free population allocated memory */
    for(int i = 0; i < popSize; i++){
        free(island1[i]);
        free(island2[i]);
        free(island3[i]);
        free(island4[i]);
    }
    free(island1);
    free(island2);
    free(island3);
    free(island4);
}

/*
 * Solves an instance of GA,
 * returning the best individual
 * This method uses the island solve
 */
void DefaultGA::islandSolve(int max_time_seconds){
    /* splits the population in  groups */
    int popSize = (int)DefaultGA::popSize/4;

    /* limitation: popSize must be pair */
    if(popSize % 2)
        popSize += 1;

    int chromosomeSize = DefaultGA::chromosomeSize;
    int generations = DefaultGA::generations;

    int **island1;
    int **island2;
    int **island3;
    int **island4;

    island1 = (int**)malloc(sizeof(int*) * popSize);
    island2 = (int**)malloc(sizeof(int*) * popSize);
    island3 = (int**)malloc(sizeof(int*) * popSize);
    island4 = (int**)malloc(sizeof(int*) * popSize);

#pragma omp taskloop
    for(int i = 0; i < popSize; i++){
        island1[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        island2[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        island3[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        island4[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }

    DefaultGA::generateInitialPopulation(island1, popSize);
    DefaultGA::generateInitialPopulation(island2, popSize);
    DefaultGA::generateInitialPopulation(island3, popSize);
    DefaultGA::generateInitialPopulation(island4, popSize);
    int *bestChromossomeInIsland1 = island1[0];
    int *bestChromossomeInIsland2 = island2[0];
    int *bestChromossomeInIsland3 = island3[0];
    int *bestChromossomeInIsland4 = island4[0];

    /* generates the inicial population */
    /* and allocates memory for offspring calculations */
    int **offspring1 = (int**)malloc(popSize * sizeof(int*));
    int **offspring2 = (int**)malloc(popSize * sizeof(int*));
    int **offspring3 = (int**)malloc(popSize * sizeof(int*));
    int **offspring4 = (int**)malloc(popSize * sizeof(int*));

    #pragma omp taskloop
    for(int i = 0; i < popSize; i++){
        offspring1[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        offspring2[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        offspring3[i] = (int*)malloc(sizeof(int) * chromosomeSize);
        offspring4[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }

    /*
     * Variables to control elapsed time
     * and elapsed number of generations
     * */
    time_t start_time,current_time;
    time (&start_time);
    int elapsed = 0;
    int elapsed_gens = 0;

    for(int i = 0; i < generations/2 && elapsed <= max_time_seconds; i++){
        /* run the generation method */
        #pragma omp parallel
        {
            #pragma omp single
            {
                #pragma omp task
                {
                    runIslandGeneration(island1, offspring1, popSize, bestChromossomeInIsland1);
                }
                #pragma omp task
                {
                    runIslandGeneration(island2, offspring2, popSize, bestChromossomeInIsland2);
                }
                #pragma omp task
                {
                    runIslandGeneration(island3, offspring3, popSize, bestChromossomeInIsland3);
                }

                #pragma omp task
                {
                    runIslandGeneration(island4, offspring4, popSize, bestChromossomeInIsland4);
                }

        }

    }
        /*  update elapsed time and number of gens */
        time (&current_time);
        elapsed = (int) difftime (current_time,start_time);
        elapsed_gens = i;

}
    /* does a migration */
    fourIslandsMigration(island1, island2, island3, island4, popSize);

    for(int i = generations/2; i < generations && elapsed <= max_time_seconds; i++){
        /* run the generation method */
        #pragma omp parallel
        {
            #pragma omp single
            {
                #pragma omp task
                {
                    runIslandGeneration(island1, offspring1, popSize, bestChromossomeInIsland1);
                }
                #pragma omp task
                {
                    runIslandGeneration(island2, offspring2, popSize, bestChromossomeInIsland2);
                }
                #pragma omp task
                {
                    runIslandGeneration(island3, offspring3, popSize, bestChromossomeInIsland3);
                }

                #pragma omp task
                {
                runIslandGeneration(island4, offspring4, popSize, bestChromossomeInIsland4);
                }

            }

        }

        /*  update elapsed time and number of gens */
        time (&current_time);
        elapsed = (int) difftime (current_time,start_time);
        elapsed_gens = i;
    }

    DefaultGA::passedGens = elapsed_gens;

    memcpy(DefaultGA::bestChromossome, DefaultGA::getBestChromossomeOf4islands(island1, island2, island3, island4, popSize),
           DefaultGA::chromosomeSize*sizeof(int));

    /* free offspring allocated memory */
#pragma omp taskloop
    for(int i = 0; i < popSize; i++){
        free(offspring1[i]);
        free(offspring2[i]);
        free(offspring3[i]);
        free(offspring4[i]);
    }
    free(offspring1);
    free(offspring2);
    free(offspring3);
    free(offspring4);

    /* free population allocated memory */
#pragma omp taskloop
    for(int i = 0; i < popSize; i++){
        free(island1[i]);
        free(island2[i]);
        free(island3[i]);
        free(island4[i]);
    }
    free(island1);
    free(island2);
    free(island3);
    free(island4);
}
